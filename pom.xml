<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.2</version>
        <relativePath/>
    </parent>

    <groupId>com.wlcb</groupId>
    <artifactId>jpower</artifactId>
    <version>2.0.0</version>

    <packaging>pom</packaging>

    <properties>
        <java.version>1.8</java.version>
        <com.jpower.version>2.0.0</com.jpower.version>
        <spring-test.version>5.2.6.RELEASE</spring-test.version>

        <springboot.data.redis.version>1.5.8.RELEASE</springboot.data.redis.version>
        <mybatis-plus.version>3.4.2</mybatis-plus.version>
        <easy-captcha.version>1.6.2</easy-captcha.version>
        <druid-spring.version>1.2.5</druid-spring.version>
        <spring.context.version>5.2.5.RELEASE</spring.context.version>
        <fastjson.version>1.2.75</fastjson.version>
        <pagehelper-spring.version>1.3.0</pagehelper-spring.version>
        <javax.mail.version>1.6.2</javax.mail.version>
        <jwt.version>0.6.0</jwt.version>
        <httpclient.version>4.5.3</httpclient.version>
        <commons-httpclient.version>3.1</commons-httpclient.version>
        <dom4j.version>1.6.1</dom4j.version>
        <guava.version>25.0-jre</guava.version>
        <poi.version>3.17</poi.version>
        <knife4j.version>2.0.8</knife4j.version>
        <swagger-annotations.version>1.6.2</swagger-annotations.version>
        <swagger.version>2.10.5</swagger.version>
        <common-io.version>2.8.0</common-io.version>
        <skywalking.version>8.1.0</skywalking.version>
        <spring-boot-admin.version>2.4.0</spring-boot-admin.version>
        <logstash.version>6.2</logstash.version>
        <netty.version>4.1.45.Final</netty.version>
        <srping-alibaba.version>2.2.5.RELEASE</srping-alibaba.version>
        <srping-cloud.version>2020.0.1</srping-cloud.version>

        <dockerfile-maven-plugin.version>1.4.13</dockerfile-maven-plugin.version>
        <docker.image.prefix>jpower</docker.image.prefix>
        <docker.registry>219.148.186.235:99</docker.registry>

    </properties>

    <modules>
        <module>jpower-module-common</module>
        <module>jpower-core-login</module>
        <module>jpower-module-gateway</module>
        <module>jpower-boot</module>
        <module>jpower-core-server</module>
        <module>jpower-op-server</module>
    </modules>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <version>${spring.context.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${srping-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${srping-alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-server</artifactId>
                <version>${spring-boot-admin.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>

    <build>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <configuration>
                        <classifier>exec</classifier>
                        <fork>true</fork>
                        <!--生成可执行jar的名称-->
                        <finalName>${project.build.finalName}</finalName>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- war包解析 -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <version>3.0.0</version>
                    <configuration>
                        <failOnMissingWebXml>false</failOnMissingWebXml>
                        <warName>${project.build.finalName}</warName>
                    </configuration>
                </plugin>
                <!-- dockerfile -->
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>dockerfile-maven-plugin</artifactId>
                    <version>${dockerfile-maven-plugin.version}</version>
                    <configuration>
                        <useMavenSettingsForAuth>true</useMavenSettingsForAuth>
                        <repository>${docker.registry}/${docker.image.prefix}/${project.artifactId}</repository>
                        <tag>${project.version}</tag>
                        <buildArgs>
                            <JAR_FILE>target/${project.build.finalName}-exec.jar</JAR_FILE>
                        </buildArgs>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>

            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.spotify</groupId>
                <artifactId>dockerfile-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <configuration>
                    <delimiters>@</delimiters>
                    <useDefaultDelimiters>false</useDefaultDelimiters>
                </configuration>
            </plugin>
        </plugins>
        <resources>
            <!-- 编译 src/main/java 目录下的 mapper 文件 -->
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
                <filtering>false</filtering>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
            <!-- ......用于扫描 dao 文件下的mapper 文件................. end -->
        </resources>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>
    </build>

</project>