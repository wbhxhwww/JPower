package com.wlcb.jpower.dbs.dao.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.role.TbCoreRole;
import org.springframework.stereotype.Component;

/**
 * @author mr.gmac
 */
@Component("tbCoreRoleMapper")
public interface TbCoreRoleMapper extends BaseMapper<TbCoreRole> {
}
