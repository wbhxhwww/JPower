package com.wlcb.jpower.dbs.dao.tenant.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.tenant.TbCoreTenant;
import org.springframework.stereotype.Component;

@Component
public interface TbCoreTenantMapper extends BaseMapper<TbCoreTenant> {
}
