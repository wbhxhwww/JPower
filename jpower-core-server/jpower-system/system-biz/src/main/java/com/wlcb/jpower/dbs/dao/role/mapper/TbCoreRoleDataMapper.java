package com.wlcb.jpower.dbs.dao.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.role.TbCoreRoleData;
import org.springframework.stereotype.Component;

/**
 * @author ding
 * @description
 * @date 2020-11-03 14:58
 */
@Component
public interface TbCoreRoleDataMapper extends BaseMapper<TbCoreRoleData> {
}
