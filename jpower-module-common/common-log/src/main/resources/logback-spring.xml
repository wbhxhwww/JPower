<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false">

    <springProfile name="dev">
        <springProperty scope="local" name="DESTINATION" source="jpower.dev.elk.destination" />
    </springProfile>
    <springProfile name="test">
        <springProperty scope="local" name="DESTINATION" source="jpower.test.elk.destination" />
    </springProfile>
    <springProfile name="prod">
        <springProperty scope="local" name="DESTINATION" source="jpower.prod.elk.destination"/>
    </springProfile>

    <springProperty scope="context" name="appName" source="spring.application.name" />

    <if condition='isNull("DESTINATION")'>
        <then>
            <property name="ELK_MODE" value="FALSE" />
            <property name="LOG_APPENDER" value="log" />
            <property name="ERROR_APPENDER" value="error" />
        </then>
        <else>
            <property name="ELK_MODE" value="TRUE" />
            <property name="LOG_APPENDER" value="log_logstash" />
            <property name="ERROR_APPENDER" value="error_logstash" />
        </else>
    </if>

    <!--定义日志文件的存储地址 勿在 LogBack 的配置中使用相对路径-->
    <property name="log.base" value="./logs" />

    <!-- 彩色日志 -->
    <!-- 彩色日志依赖的渲染类 -->
    <conversionRule conversionWord="clr" converterClass="org.springframework.boot.logging.logback.ColorConverter" />
    <conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
    <conversionRule conversionWord="wEx" converterClass="org.springframework.boot.logging.logback.ExtendedWhitespaceThrowableProxyConverter" />
    <!-- 彩色日志格式 -->
    <property name="CONSOLE_LOG_PATTERN" value="${CONSOLE_LOG_PATTERN:-%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}}"/>

    <!-- 文件日志格式-->
    <property name="patternAsyn" value="%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level [%tid] [%thread] [%C.java:%L] - %msg%n" />

    <!--控制台日志， 控制台输出 -->
    <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <Pattern>${CONSOLE_LOG_PATTERN}</Pattern>
            <!-- 设置字符集 -->
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <appender name="log" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${log.base}/log/log.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--日志文件输出的文件名-->
            <FileNamePattern>${log.base}/log/log-%d{yyyyMMdd}.log</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
        </rollingPolicy>
        <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
            <layout class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.TraceIdPatternLogbackLayout">
                <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
                <pattern>${patternAsyn}</pattern>
            </layout>
            <charset>UTF-8</charset>
        </encoder>
        <!-- 打印日志级别 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>INFO</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
    </appender>

    <appender name="error" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${log.base}/error/error.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!--日志文件输出的文件名-->
            <FileNamePattern>${log.base}/error/error-%d{yyyyMMdd}.log</FileNamePattern>
            <!--日志文件保留天数-->
            <MaxHistory>30</MaxHistory>
        </rollingPolicy>
	    <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>error</level>
        </filter>
        <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
            <layout class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.TraceIdPatternLogbackLayout">
                <!--格式化输出：%d表示日期，%thread表示线程名，%-5level：级别从左显示5个字符宽度%msg：日志消息，%n是换行符-->
                <pattern>${patternAsyn}</pattern>
            </layout>
            <charset>UTF-8</charset>
        </encoder>
    </appender>

    <conversionRule conversionWord="tid" converterClass="org.apache.skywalking.apm.toolkit.log.logback.v1.x.LogbackPatternConverter"/>

    <if condition='property("ELK_MODE").toUpperCase().contains("TRUE")'>
        <then>
            <!-- 推送日志至elk -->
            <appender name="log_logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
                <destination>${DESTINATION}</destination>
                <!-- 日志输出编码 -->
                <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder">
                    <provider class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.logstash.TraceIdJsonProvider">
                    </provider>
                    <customFields>{"pid":"${PID}"}</customFields>
                </encoder>
                <!-- 打印日志级别 -->
                <filter class="ch.qos.logback.classic.filter.LevelFilter">
                    <level>INFO</level>
                    <onMatch>ACCEPT</onMatch>
                    <onMismatch>DENY</onMismatch>
                </filter>
            </appender>

            <!-- 推送日志至elk -->
            <appender name="error_logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
                <destination>${DESTINATION}</destination>
                <!-- 日志输出编码 -->
                <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder">
                    <provider class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.logstash.TraceIdJsonProvider">
                    </provider>
                    <customFields>{"pid":"${PID}"}</customFields>
                </encoder>
                <!-- 打印日志级别 -->
                <filter class="ch.qos.logback.classic.filter.LevelFilter">
                    <level>ERROR</level>
                    <onMatch>ACCEPT</onMatch>
                    <onMismatch>DENY</onMismatch>
                </filter>
            </appender>
        </then>
    </if>

    <!--myibatis log configure-->
    <logger name="com.apache.ibatis" level="info"/>
    <logger name="com.ibatis" level="info" />
    <logger name="com.ibatis.common.jdbc.SimpleDataSource" level="info" />
    <logger name="com.ibatis.common.jdbc.ScriptRunner" level="info" />
    <logger name="com.ibatis.sqlmap.engine.impl.SqlMapClientDelegate" level="info" />
    <logger name="java.sql.Connection" level="info" />
    <logger name="java.sql.Statement" level="info" />
    <logger name="java.sql.PreparedStatement" level="info" />
    <!-- 减少nacos日志 -->
    <logger name="com.alibaba.nacos" level="ERROR"/>

    <logger name="com.wlcb.jpower.dbs.dao" level="DEBUG" />

    <!-- 日志输出级别 -->
    <springProfile name="dev">
        <root level="debug">
            <appender-ref ref="${LOG_APPENDER}"/>
            <appender-ref ref="${ERROR_APPENDER}"/>
            <appender-ref ref="stdout" />
        </root>
    </springProfile>
    <springProfile name="!dev">
        <root level="info">
            <appender-ref ref="${LOG_APPENDER}"/>
            <appender-ref ref="${ERROR_APPENDER}"/>
            <appender-ref ref="stdout" />
        </root>
    </springProfile>
</configuration>
