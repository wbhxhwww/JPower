package com.wlcb.jpower.module.common.utils.constants;

/**
 * @ClassName ParamsConstants
 * @Description TODO 系统参数CODE值
 * @Author 郭丁志
 * @Date 2020-07-31 21:49
 * @Version 1.0
 */
public class ParamsConstants {

    /** 系统默认登录密码 **/
    public static final String USER_DEFAULT_PASSWORD = "JPOWER_USER_DEFAULT_PASSWORD";
    /** 新增用户默认是否激活 **/
    public static final String IS_ACTIVATION = "JPOWER_IS_ACTIVATION";
    /** 白名单地址 **/
    public static final String IP_LIST = "JPOWER_IP_LIST";

}
