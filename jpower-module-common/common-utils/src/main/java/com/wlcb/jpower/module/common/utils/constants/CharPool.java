package com.wlcb.jpower.module.common.utils.constants;


/**
 * char 常量池
 * @author mr.gmac
 */
public interface CharPool {

    char UPPER_A		= 'A';
    char LOWER_A		= 'a';
    char UPPER_Z		= 'Z';
    char LOWER_Z		= 'z';
    char NUMBER_0		= '0';
    char NUMBER_9		= '9';
    char DOT			= '.';
    char AT				= '@';
    char LEFT_BRACE		= '{';
    char RIGHT_BRACE	= '}';
    char LEFT_BRACKET	= '(';
    char RIGHT_BRACKET	= ')';

}
