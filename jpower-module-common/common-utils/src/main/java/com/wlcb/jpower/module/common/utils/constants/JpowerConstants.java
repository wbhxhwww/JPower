package com.wlcb.jpower.module.common.utils.constants;


/**
 * @ClassName SystemConstants
 * @Description TODO 系统常量
 * @Author 郭丁志
 * @Date 2020-07-23 11:23
 * @Version 1.0
 */
public class JpowerConstants {

    /** 顶级节点CODE **/
    public static final String TOP_CODE = "-1";

    /** 默认国家编码 **/
    public static final String COUNTRY_CODE = "CHN";

    /** 默认数据库业务状态 **/
    public static final Integer DB_STATUS_NORMAL = 1;

    /** 数据库删除状态 {0=未删除，1=删除} **/
    public static final Integer DB_NOT_DELETE = 0;
    public static final Integer DB_IS_DELETE = 1;

    /** 框架版本 **/
    public static final String JPOWER_VESION = "2.0.0";

}
