package com.wlcb.jpower.dbs.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.TbLogMonitorSetting;
import org.springframework.stereotype.Component;

/**
 * @Author mr.g
 * @Date 2021/4/19 0019 18:46
 */
@Component("logMonitorSettingMapper")
public interface LogMonitorSettingMapper extends BaseMapper<TbLogMonitorSetting> {
}
