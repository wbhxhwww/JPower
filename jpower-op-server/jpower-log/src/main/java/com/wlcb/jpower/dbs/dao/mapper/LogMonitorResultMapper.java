package com.wlcb.jpower.dbs.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.dbs.entity.TbLogMonitorResult;
import org.springframework.stereotype.Component;

/**
 * @author mr.g
 * @date 2021-04-07 16:12
 */
@Component("logMonitorResultMapper")
public interface LogMonitorResultMapper extends BaseMapper<TbLogMonitorResult> {
}
